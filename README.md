### Installation

There is a `docker-compose.yml` file for starting Docker.

`docker-compose up`

This command will install node modules and run up the server. Also at first boot, will populate the database.

### Using API

There is swagger to test the API. the following Bearer JWT Token can authorize to use the API token

`eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoicmVzdHJpY3RlZCIsImlhdCI6MTYzMzI1Mjg0MiwiaXNzIjoicmVpZ24ifQ.u1d6gcorhQ5okbxiCtzpYvBD4kuSdTtl1dQWqihhvXo`