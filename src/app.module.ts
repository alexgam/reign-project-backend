import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { config } from './config';
import { ScheduleModule } from '@nestjs/schedule';
import { HitsModule } from './hits/hits.module';
import { PaginateModule } from './paginate/paginate.module';

@Module({
  imports: [
    ConfigModule.forRoot({isGlobal:true}),
    MongooseModule.forRoot(config.mongo.uri, config.mongo.config),
    ScheduleModule.forRoot(),
    HitsModule,
    PaginateModule
  ],
})
export class AppModule {}
