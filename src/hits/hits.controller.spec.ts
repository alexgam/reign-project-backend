import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import * as jwt from 'jsonwebtoken';

import { AppModule } from '../app.module';

describe('HitsModule', () => {
  let app: INestApplication;

  beforeAll(async () => {

    const module = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  afterAll(async () =>
    app.close()
  );

  it('should return an array of hits', async () => {
    const token = jwt.sign({ role: 'restricted' }, `${process.env.JWT_SECRET}`, {
      algorithm: 'HS256',
      issuer: 'reign'
    });

    console.log(token);

    return request(app.getHttpServer())
      .get('/hits')
      .set('Authorization', `Bearer ${token}`)
      .expect(HttpStatus.OK)
      .then(response => {
        expect(response.body).toBeInstanceOf(Array);
      })
  });
});
