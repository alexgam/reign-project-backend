import { forwardRef, Module } from '@nestjs/common';
import { HitsController } from './hits.controller';
import { HitsService } from './hits.service';
import { HttpModule } from '@nestjs/axios';

// schema
import { Hit, HitSchema } from './hits.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { PaginateModule } from '../paginate/paginate.module';

@Module({
  imports: [
    forwardRef(()=>PaginateModule),
    MongooseModule.forFeature([{ name: Hit.name, schema: HitSchema }]),
    HttpModule
  ],
  controllers: [HitsController],
  providers: [HitsService],
  exports:[HitsService]
})
export class HitsModule {}
