import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HttpService } from '@nestjs/axios';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Hit } from './hits.schema';

@Injectable()
export class HitsService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Hit.name) private readonly model: Model<Hit>,
  ) { }

  async onApplicationBootstrap(): Promise<void> {
    console.log("start to insert");
    await this.getHackerNews();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async refreshNews(): Promise<void> {
    await this.getHackerNews();
    console.log('Hits refreshed');
  }

  async getHackerNews(): Promise<void> {
    const monthNames = ["january", "february", "march", "april", "may", "june",
      "july", "august", "september", "october", "november", "december"
    ];

    await this.httpService
      .get<{ hits: Hit[] }>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe((res) => {
        res.data.hits.forEach(async (article) => {
          article._id = parseInt(article.objectID);
          const d = new Date(article.created_at);
          article.month = monthNames[d.getMonth()];

          try {

            const articleExist = await this.model.exists({ _id: article._id }); // { _id: ... }

            if (!articleExist) {
              await this.model.create(article);
            }
          } catch (error) {
            console.log(error);
          }
        });
      });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  findAll(options: Record<string, unknown>) {
    return this.model.find(options).sort({ 'created_at': 'desc' });
  }

  async count(options: Record<string, unknown>): Promise<number> {
    return await this.model.count(options).exec();
  }
}
