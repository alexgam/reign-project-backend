import {
  Controller, Get, UseGuards, Query, Inject, forwardRef
} from '@nestjs/common';
import { PaginateService, PaginationQuery } from '../paginate';
import { RestrictedGuard } from '../common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@Controller('hits')
export class HitsController {
  constructor(
    @Inject(forwardRef(() => PaginateService))
    private readonly paginageService: PaginateService) { }

  @Get()
  @ApiBearerAuth('JWT')
  @ApiTags('hits')
  @UseGuards(RestrictedGuard)
  async index(@Query() params: PaginationQuery = new PaginationQuery()): Promise<Record<string, unknown>> {
    return await this.paginageService.hitsPaginate(params);
  }
}
