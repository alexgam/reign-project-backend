import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Hit extends Document {
  @Prop() _id: number;

  @Prop() created_at: string;

  @Prop() title: string;

  @Prop() url: string;

  @Prop() author: string;

  @Prop() comment_text: string;

  @Prop() story_id: number;

  @Prop() story_title: string;

  @Prop() story_url: string;

  @Prop() num_comments: number;

  @Prop() points: number;

  @Prop() parent_id: number;

  @Prop() objectID: string;

  @Prop() _tags: string[];

  @Prop() story_text: string;

  @Prop() month: string;
}

export const HitSchema = SchemaFactory.createForClass(Hit);