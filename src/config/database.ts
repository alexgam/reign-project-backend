const config = {
  mongo: {
    uri: `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    config: {
      authSource: 'admin',
    },
  }
}
export default config;