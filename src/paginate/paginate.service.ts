import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { HitsService } from '../hits/hits.service';
import { PaginationQuery } from './paginate.query';

const current_page = 1;
const per_page = 5;

@Injectable()
export class PaginateService {

    constructor(
        @Inject(forwardRef(() => HitsService))
        private readonly hitsService: HitsService,
    ) { }

    hitsPaginate = async (params: PaginationQuery): Promise<Record<string, unknown>> => {

        const options = this.generateUserQuery(params);

        const query = this.hitsService.findAll(options);

        const page: number = parseInt(params.page as any) || current_page;
        const limit: number = parseInt(params.limit as any) || per_page;
        const total = await this.hitsService.count(options);

        const data = await query.skip((page - 1) * limit).limit(limit).exec();

        return {
            data,
            total,
            page,
            last_page: Math.ceil(total / limit)
        };
    }

    private generateUserQuery = (params: PaginationQuery) => {

        let options = {};
        const and: any[] = [];
        const or: any[] = [];

        if (params.author) {
            or.push(
                { author: new RegExp(params.author.toString(), 'i') }
            );
        };

        if (params.tag) {
            or.push({ tag: params.tag });
        };

        if (params.month) {
            or.push({ month: params.month.toLowerCase() });
        };


        if (or.length > 0 && and.length > 0) {
            options = {
                $or: or,
                $and: and
            };
        } else {

            if (or.length > 0) {
                options = {
                    $or: or
                };
            }

            if (and.length > 0) {
                options = {
                    $and: and
                };
            }
        }

        return options;
    };
}
