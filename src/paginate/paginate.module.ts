import { forwardRef, Module } from '@nestjs/common';
import { HitsModule } from '../hits/hits.module';
import { PaginateService } from './paginate.service';

@Module({
  imports: [
    forwardRef(()=> HitsModule),
  ],
  providers: [PaginateService],
  exports:[PaginateService]
})
export class PaginateModule {}
