import { ApiProperty } from '@nestjs/swagger';

export class PaginationQuery {
    @ApiProperty({
        minimum: 1,
        title: 'Page',
        exclusiveMinimum: true,
        format: 'int32',
        default: 1,
        required: false
    })
    page: number;

    @ApiProperty({
        required: false
    })
    limit: number;

    @ApiProperty({
        type: 'string',
        required: false
    })
    author: string;

    @ApiProperty({
        type: 'string',
        required: false
    })
    tag: string;

    @ApiProperty({
        type: 'string',
        required: false
    })
    month: string;
}